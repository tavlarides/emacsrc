(require 'misc)


;; Copy current line when there is no selection
(defadvice kill-ring-save (before slick-copy activate compile)
  "When called interactively with no active region, copy a single line instead."
  (interactive
   (if mark-active (list (region-beginning) (region-end))
     (message "Copied line")
     (list (line-beginning-position)
           (line-beginning-position 2)))))

;; Kill current line when there is no selection
(defadvice kill-region (before slick-cut activate compile)
  "When called interactively with no active region, kill a single line instead."
  (interactive
   (if mark-active (list (region-beginning) (region-end))
     (list (line-beginning-position)
           (line-beginning-position 2)))))

;; comment region
(defun my/comment-or-uncomment-region-or-line ()
    "Comments or uncomments the region or the current line if there's no active region."
    (interactive)
    (let (beg end)
        (if (use-region-p)
            (setq beg (region-beginning) end (region-end))
            (setq beg (line-beginning-position) end (line-end-position)))
        (comment-or-uncomment-region beg end)))

;; jump to forward in mark ring
(defun my/unpop-to-mark-command ()
	"Unpop off mark ring. Does nothing if mark ring is empty."
	(interactive)
	(when mark-ring
		(let ((pos (marker-position (car (last mark-ring)))))
			(if (not (= (point) pos))
					(goto-char pos)
				(setq mark-ring (cons (copy-marker (mark-marker)) mark-ring))
				(set-marker (mark-marker) pos)
				(setq mark-ring (nbutlast mark-ring))
				(goto-char (marker-position (car (last mark-ring))))))))

;; begin searching for the word at point
(defun my/search-word-at-point-or-region (arg)
	"Begin searching for word at point or selection if any"
	(interactive "p")
	(let (selection)
		(setq selection
          (if (region-active-p)
							(buffer-substring (region-beginning) (region-end))
						(thing-at-point 'word)))
		(deactivate-mark)
		(isearch-mode t nil nil nil)
		(isearch-yank-string selection)))

;; duplicate current line or region
(defun my/duplicate-line-or-region (arg)
	"My first attempt to duplicate current line or region"
	(interactive "p")
	(let (beg end)
		(if (region-active-p)
				(setq beg (region-beginning) end (region-end))
			(setq beg (line-beginning-position) end (line-end-position)))
		(save-excursion
			(let ((selection (buffer-substring-no-properties beg end)))
				(dotimes (i arg)
					(beginning-of-line)
					(open-line 1)
					(insert selection))))))

;; open a new line below current line
(defun my/open-line (&optional arg)
  "Opens line(s) vim's way"
  (interactive "p")
  (move-end-of-line nil)
  (newline arg)
  (indent-according-to-mode))

;; open a new line above current line
(defun my/open-line-above (&optional arg)
  "Opens above line(s) vim's way"
  (interactive "p")
  (move-beginning-of-line nil)
  (open-line arg)
  (indent-according-to-mode))

;; browse via ido for recent files
(defun my/ido-recentf-open ()
  "Use `ido-completing-read' to \\[find-file] a recent file"
  (interactive)
  (if (find-file (ido-completing-read "Find recent file: " recentf-list))
      (message "Opening file...")
    (message "Aborting")))

;; make meta-f to place the point on begin instead of the end of word
(define-key global-map [(meta ?f)] 'forward-to-word)
(define-key global-map [(meta right)] 'forward-to-word)

(define-key global-map [(super ?e)] 'my/search-word-at-point-or-region)
(define-key global-map [(super ?g)] (lambda () (interactive) (isearch-repeat-forward) (recenter)))
(define-key global-map [(super ?G)] (lambda () (interactive) (isearch-repeat-backward) (recenter)))

(define-key global-map [(super ?c)]       'kill-ring-save)
(define-key global-map [(super ?x)]       'kill-region)
(define-key global-map [(super ?v)]       'yank)
(define-key global-map [(super shift ?v)] 'yank-pop)

(define-key global-map [(super ?.)] 'my/unpop-to-mark-command)
(define-key global-map [(super ?,)] (lambda () (interactive) (set-mark-command 1)))

(define-key global-map [(super ?d)]       'my/duplicate-line-or-region)
(define-key global-map [(super ?/)]       'my/comment-or-uncomment-region-or-line)
(define-key global-map [(super ?o)]       'my/open-line)
(define-key global-map [(super shift ?o)] 'my/open-line-above)

(define-key global-map [(super ?z)]       'undo-tree-undo)
(define-key global-map [(super shift ?z)] 'undo-tree-redo)

(define-key global-map [(control ?z)] 'zap-up-to-char)

(define-key global-map [(control ?x) (control ?r)] 'my/ido-recentf-open)

(define-key global-map [(control tab)]       'switch-to-next-buffer)
(define-key global-map [(control shift tab)] 'switch-to-prev-buffer)

(define-key global-map [(super ?\[)] 'backward-sexp)
(define-key global-map [(super ?\])] 'forward-sexp)
