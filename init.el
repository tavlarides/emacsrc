;; This is my emacs configuration
(require 'cask "/usr/local/Cellar/cask/0.7.2/cask.el")
(cask-initialize)


;; make parentheses look more subtle
(defface paren-face
  '((((class color) (background dark))
     (:foreground "grey30"))
    (((class color) (background light))
     (:foreground "grey80")))
  "Face used to dim parentheses.")

(defun dim-parens () (font-lock-add-keywords nil '(("(\\|)" . 'paren-face))))

(add-hook 'emacs-lisp-mode-hook 'dim-parens)
(add-hook 'clojure-mode-hook 'dim-parens)
(add-hook 'lisp-mode-hook 'dim-parens)

(when (require 'diminish nil 'noerror)
  (eval-after-load "company"
    '(diminish 'company-mode " c"))
  (eval-after-load "abbrev"
    '(diminish 'abbrev-mode " a"))
  (eval-after-load "yasnippet"
    '(diminish 'yas/minor-mode " y"))
  (eval-after-load "aggressive-indent"
    '(diminish 'aggressive-indent-mode " ➔"))
  (eval-after-load "undo-tree"
    '(diminish 'undo-tree-mode))
  (eval-after-load "git-gutter"
    '(diminish 'git-gutter-mode " ⎇ ")))

(add-hook 'emacs-lisp-mode-hook
          (lambda()
            (setq mode-name " λ")))

(setq projectile-mode-line '(:eval (format " [%s]" (projectile-project-name))))

;; future packages
;; *) yasnippet
;; *) rebox2

(require 'helm)
(require 'helm-config)
;; The default "C-x c" is quite close to "C-x C-c", which quits Emacs.
;; Changed to "C-c h". Note: We must set "C-c h" globally, because we
;; cannot change `helm-command-prefix-key' once `helm-config' is loaded.
(global-set-key (kbd "C-c h") 'helm-command-prefix)
(global-unset-key (kbd "C-x c"))

(define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action) ; rebind tab to run persistent action
(define-key helm-map (kbd "C-i") 'helm-execute-persistent-action) ; make TAB works in terminal
(define-key helm-map (kbd "C-z")  'helm-select-action) ; list actions using C-z

(when (executable-find "curl")
  (setq helm-google-suggest-use-curl-p t))

(setq helm-split-window-in-side-p           t ; open helm buffer inside current window, not occupy whole other window
      helm-move-to-line-cycle-in-source     t ; move to end or beginning of source when reaching top or bottom of source.
      helm-ff-search-library-in-sexp        t ; search for library in `require' and `declare-function' sexp.
      helm-scroll-amount                    8 ; scroll 8 lines other window using M-<next>/M-<prior>
      helm-ff-file-name-history-use-recentf t)

(helm-mode 1)

(require 'git-gutter)
(global-git-gutter-mode 't)
(global-set-key (kbd "C-x C-g") 'git-gutter:toggle)
(global-set-key (kbd "C-x p")   'git-gutter:previous-hunk)
(global-set-key (kbd "C-x n")   'git-gutter:next-hunk)
(global-set-key (kbd "C-x v s") 'git-gutter:stage-hunk)
(global-set-key (kbd "C-x v r") 'git-gutter:revert-hunk)

(add-hook 'js-mode-hook 'js2-minor-mode)

(setq auto-hscroll-mode t
      auto-save-default nil
      column-number-mode t
      dired-details-hide-link-targets nil
      dired-dwim-target t
      dired-recursive-copies (quote always)
      disabled-command-function nil
      mac-emulate-three-button-mouse t ;M-mouse2 on spelling errors shows popup
      gui-select-enable-clipboard t
      hscroll-margin 1
      hscroll-step 1
      ido-cannot-complete-command (quote ido-next-match)
      ido-enable-flex-matching t
      ido-everywhere t
      ido-separator nil
      ido-use-filename-at-point 'guess
      indicate-empty-lines nil
      inhibit-startup-buffer-menu t
      inhibit-startup-echo-area-message t
      inhibit-startup-screen t
      initial-scratch-message nil
      ispell-program-name "/usr/local/bin/aspell"
      js2-basic-offset 2
      kill-do-not-save-duplicates t
      kill-whole-line t
      magit-use-overlays nil
      make-backup-files nil
      mac-option-modifier 'meta
      mac-command-modifier 'super
      recentf-max-menu-items 25
      recentf-max-saved-items 50
      ring-bell-function 'ignore
      save-place t
      scroll-conservatively 100000
      scroll-error-top-bottom t
      scroll-margin 1
      sentence-end-double-space nil
      set-mark-command-repeat-pop t
      size-indication-mode t
      standard-indent 2
      trailing-show-whitespace t
      user-full-name "Tassos Tavlarides"
      user-mail-address "tavlarides@gmail.com"
      whitespace-display-mappings '(
                                    (space-mark   ?\     [?·])
                                    (newline-mark ?\n    [?← ?\n])
                                    (tab-mark     ?\t    [?→ ?\t])
                                    )
      whitespace-style (quote (face trailing spaces tabs newline space-mark tab-mark newline-mark)))

(setq-default tab-width 2
              cursor-type (quote (bar . 1))
              indent-tabs-mode nil      ;use spaces for indentation
              truncate-lines t)

(require 'dired)
(require 'undo-tree)
(require 'expand-region)
(require 'change-inner)
(require 'multiple-cursors)
(require 'move-text)

(move-text-default-bindings)

;;
;; ace jump mode major function
;;
(autoload
  'ace-jump-mode
  "ace-jump-mode"
  "Emacs quick move minor mode"
  t)

(projectile-global-mode)

(define-key global-map [remap list-buffers] 'buffer-menu) ; use buffer-menu instead of list-buffers

;; keystrokes delete the selection (region)
(delete-selection-mode 1)

;; do not create buffers for every directory I visit
(define-key dired-mode-map [(return)] 'dired-find-alternate-file)
(define-key dired-mode-map [?^] (lambda () (interactive) (find-alternate-file "..")))

;; disable tooltips
(tooltip-mode -1)

;; highlight closing parenthesis
(show-paren-mode t)

;; Change window by pressing M-arrows
(windmove-default-keybindings 'control)

;; Enable auto close pairs
(electric-pair-mode t)

;; Automatic indent
;;(global-aggressive-indent-mode t)

;; company mode
(add-hook 'after-init-hook 'global-company-mode)

(ido-mode 1)

;; enable recent files mode.
(recentf-mode t)

;; undo tree
(global-undo-tree-mode)

(fset 'yes-or-no-p 'y-or-n-p)

(define-key global-map [(control ?<)]                          'mc/mark-previous-like-this)
(define-key global-map [(control ?>)]                          'mc/mark-next-like-this)
(define-key global-map [(control ?c) (control ?<)]             'mc/mark-all-like-this)
(define-key global-map [(shift control ?c) (shift control ?c)] 'mc/edit-lines) ; place cursors on each selected line
(define-key global-map [(shift control ?c) (shift control ?e)] 'mc/edit-ends-of-lines) ; place cursors on each selected line end

                                        ; you may continue expanding by pressing = or shrink by pressing -
(define-key global-map [(control ?=)] 'er/expand-region)

(define-key global-map [(control ?c) (?a)] 'org-agenda)
(define-key global-map [(control ?c) (?b)] 'org-iswitchb)
(define-key global-map [(control ?c) (?c)] 'org-capture)
(define-key global-map [(control ?c) (?l)] 'org-store-link)

(define-key global-map [(meta ?i)] 'change-inner)
(define-key global-map [(meta ?o)] 'change-outer)

(define-key global-map [(meta ?x)]      'smex)

(define-key global-map [(super ?r)]     'toggle-truncate-lines)
(define-key global-map [(super down)]   'end-of-buffer)
(define-key global-map [(super left)]   'move-beginning-of-line)
(define-key global-map [(super right)]  'move-end-of-line)
(define-key global-map [(super up)]     'beginning-of-buffer)

(define-key global-map [remap list-buffers] 'buffer-menu) ; use buffer-menu instead of list-buffers

(define-key global-map [(super ?j)] 'join-line)
(define-key global-map [(super ?m)] 'ace-jump-mode)
(define-key global-map [(shift super ?m)] 'ace-jump-line-mode)
(define-key global-map [(super ?p)] 'ace-window)

(load "~/.emacs.d/myLibrary.el")

