# This is my emacs configuration. #

Files included:
* readme.org (this file)
* init.el (emacs configuration file)
* myLibrary.el (my additions to emacs)
* my-theme-theme.el (deleted)
* Cask (cask file to make package installation easier)
* .gitignore (excludes all other files from versioning)
* org.gnu.Emacs.plist
  obsoletes my-theme, no more flicker on startup
  (must be copied to ~/Library/Preferences/)
  
